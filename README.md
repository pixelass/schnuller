# Schnuller

## `[ˈʃnʊlɐ]`

When it creates too much poop give it a Schnuller to feed it with noop.

Dedicated to Nika ([Tim Pietrusky](https://github.com/timpietrusky)s daughter)


[![npm](https://img.shields.io/npm/v/schnuller.svg?style=flat-square)](https://www.npmjs.com/package/schnuller)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-44aa44.svg?style=flat-square)](https://github.com/conventional-changelog/standard-version)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://raw.githubusercontent.com/pixelass/schnuller/master/LICENSE)
[![GitHub issues](https://img.shields.io/github/issues/pixelass/schnuller.svg?style=flat-square)](https://github.com/pixelass/schnuller/issues)

[![Travis branch](https://img.shields.io/travis/pixelass/schnuller/master.svg?style=flat-square)](https://travis-ci.org/pixelass/schnuller)
[![bitHound](https://img.shields.io/bithound/code/github/pixelass/schnuller.svg?style=flat-square)](https://www.bithound.io/github/pixelass/schnuller)
[![bitHound](https://img.shields.io/bithound/devDependencies/github/pixelass/schnuller.svg?style=flat-square)](https://www.bithound.io/github/pixelass/schnuller)
[![Coveralls](https://img.shields.io/coveralls/pixelass/schnuller.svg?style=flat-square)](https://coveralls.io/github/pixelass/schnuller)


<p align="center"><img width="300" src="https://cdn.rawgit.com/pixelass/schnuller/master/schnuller.svg" alt="logo"/></p>

* Silence methods of an object
* Create silenced copies
* Create objects with noop methods

<!-- toc -->

- [Installation](#installation)
- [Usage](#usage)
  * [Silence `console.log`](#silence-consolelog)
  * [Silenced copies](#silenced-copies)
  * [Keep non functions](#keep-non-functions)
- [Contributing](#contributing)
  * [Testing](#testing)

<!-- tocstop -->

## Installation

```
yarn add schnuller
```

## Usage

### Silence `console.log`

```js
import Schnuller from 'schnuller'

window.console = new Schnuller(window.console, 'log')

console.log('Test') // => undefined
```

### Silenced copies

```js
import Schnuller from 'schnuller'

const foo = {
  hello(name = 'world') {
    return `Hello ${name}`
  },
  bye(name = 'world') {
    return `Bye ${name}`
  }
}
const bar = new Schnuller(foo, 'bye')
bar.hello() // => 'Hello World'
bar.bye() // => undefined
foo.hello() // => 'Hello World'
foo.bye() // => 'Bye World'
```

### Keep non functions

```js
import Schnuller from 'schnuller'

const foo = {
  hello(name = 'world') {
    return `Hello ${name}`
  },
  bye(name = 'world') {
    return `Bye ${name}`
  },
  ciao: 'ciao'
}
const bar = new Schnuller({}, 'bye', 'ciao')
bar.hello() // 'hello world'
bar.bye() // => undefined
bar.ciao() // => Error bye is not a function
```

## Contributing

### Testing

```
yarn test
```

© 2017 by [Gregor Adams](greg@pixelass.com)

Logo via [onlinewebfonts](http://www.onlinewebfonts.com)
