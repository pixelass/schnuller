/**
 * Simple noop function
 * @author Gregor Adams <greg@pixelass.com>
 * @version 1.0.0
 * @example
 * const foo = noop
 * foo() // => undefined
 * const bar = noop() // bar = undefined
 */
const noop = () => {}

/**
 * Silence methods of an object.
 * Takes the original object and a list of method names.
 * Overwrites all methods that match a name in the list
 */
class Schnuller {
  /**
   * Schnuller can silence methods of an object
   * @param {object} original
   *   The original object.
   * @param {...string} methods
   *   Method names that should be silenced
   * @returns {object}
   *   Modified original with silenced methods
   * @author Gregor Adams <greg@pixelass.com>
   * @version 1.0.0
   * @example
   * window.console = new Schnuller(window.console, 'log') // silenced console.log()
   * console.log('Whaaaa whaaaa') // => undefined
   * const x = new Schnuller({}, 'noop') // x = {noop(){}}
   */
  constructor(original, ...methods) {
    // Create a copy of the original object
    this.methods = {...original}
    // Overwrite all methods from the list
    methods.forEach(method => {
      // Check if the orinal property value is a function
      // Then noop it
      if (typeof original[method] === 'function') {
        this.methods[method] = noop
      }
    })
    // Return the modified object
    return this.methods
  }
}

export default Schnuller
