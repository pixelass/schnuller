# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.0.0"></a>
# 1.0.0 (2017-07-03)


### Features

* **schnuller:** added first version of schnuller ([22ce26a](https://github.com/pixelass/schnuller/commit/22ce26a))
