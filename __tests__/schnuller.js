import test from 'ava'
import Schnuller from '../src'

const testObject = {
  foo() {
    return 'foo'
  },
  bar() {
    return 'bar'
  },
  baz() {
    return 'baz'
  },
  qux: 'qux'
}

test('Schnuller creates objects', t => {
  const a = new Schnuller({})
  t.true(typeof a === 'object')
})

test('Schnuller can replace a method with noop', t => {
  const a = new Schnuller(testObject, 'foo')
  t.true(typeof a.foo === 'function')
  t.true(typeof a.foo() === 'undefined')
})

test('Schnuller can replace multiple methods with noop', t => {
  const a = new Schnuller(testObject, 'foo', 'bar')
  t.true(typeof a.foo === 'function')
  t.true(typeof a.bar === 'function')
  t.true(typeof a.foo() === 'undefined')
  t.true(typeof a.bar() === 'undefined')
})

test('Schnuller does not affect unlisted methods', t => {
  const a = new Schnuller(testObject, 'foo')
  t.true(typeof a.bar === 'function')
  t.true(typeof a.bar() === 'string')
  t.true(a.bar() === 'bar')
  t.true(a.qux === 'qux')
})

test('Schnuller only silences methods/functions', t => {
  const a = new Schnuller(testObject, 'foo', 'qux')
  t.true(typeof a.foo === 'function')
  t.true(typeof a.foo() === 'undefined')
  t.true(typeof a.qux === 'string')
  t.true(a.qux === 'qux')
})
